package com.bosssoft.hr.train.j2se.pojo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @ClassName: BaseEntity
 * @Description: 定义所有的entity的父类，主要声明了多个表中的公共字段，以便其他类可以继承复用
 * lombok注解@Data 自动生成标准的getter和setter方法，以及 toString()，equals()，hashCode() 等方法。
 * @Author: Xiao Yihong
 * @Date: 2023-2-19 15:14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseEntity{
    /**
     * 用户ID 主键
     */
    protected Long id;

    /**
     * 租户id
     */
    private Long tenantId;

    /**
     * 组织机构id
     */
    private Long orgId;

    /**
     * 公司id
     */
    private Long companyId;

    /**
     * 创建人id
     */
    private Long createdBy;

    /**
     * 姓名
     */
    private String creator;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改人
     */
    private Long updatedBy;

    /**
     * 修改人
     */
    private String modifier;

    /**
     * 修改时间
     */
    private Date updatedTime;

    /**
     * 状态位 正常用户为 1 注销用户为 0 锁定用户为 2
     */
    private byte status;

    /**
     *  版本
     */
    private Long version;
}

package com.bosssoft.hr.train.j2se.exception;

/**
 * @Class: BusinessException
 * @Description: 定义了一个在程序运行过程中发生的业务异常的自定义异常类
 * 自定义异常持有异常码，异常消息，原异常的信息。
 * @Author: Xiao Yihong
 * @Date: 2023/02/21 09:30
 */
public class BusinessException extends RuntimeException{
    /**
     * 错误码
     */
    private final String errorCode;

    /**
     * 自定义错误信息
     * @param code 错误码
     * @param message 错误信息提示
     * @param cause 具体异常信息
     */
    public BusinessException(String code, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = code;
    }

    /**
     * 通过枚举的方式构建异常对象
     * @param exceptionCode 枚举对象，其中包含了错误码及异常信息
     */
    public BusinessException(ExceptionCode exceptionCode){
        super(exceptionCode.getMsg());
        this.errorCode = exceptionCode.getCode();
    }
}

package com.bosssoft.hr.train.j2se.stream;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;

import java.util.*;

/**
 * @ClassName: StreamDemo
 * @Description: 演示了java作业要求中的lambda和stream结合的使用
 * @Author: Xiao Yihong
 * @Date: 2023-2-21 20:14
 */
@Slf4j
public class StreamDemo {
    /**
     * 用户用于做测试数据基本结构
     */
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class User{
        private String name;
        private Date birthday;

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", birthday=" + birthday +
                    '}';
        }
    }

    /**
     *  对应作业要求根据数组转化成list
     * @return 用于测试的list
     */
    private static List<User> createUsers() {
        List<User> users = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        cal.set(1995, Calendar.JUNE, 23);
        users.add(new User("Alice", cal.getTime()));
        cal.set(1987, Calendar.SEPTEMBER, 12);
        users.add(new User("Bob", cal.getTime()));
        cal.set(1998, Calendar.FEBRUARY, 5);
        users.add(new User("Charlie", cal.getTime()));
        cal.set(1990, Calendar.NOVEMBER, 30);
        users.add(new User("David", cal.getTime()));
        return users;
    }

    /**
     * 根据题目要求依次进行 过滤大于30 ，排序，增加年龄 以及输出操作
     * @param users 原始列表
     */
    private static void testStreamApply(List<User> users) {
        // 过滤大于30岁的用户
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -30);
        users.stream()
                .filter(user -> user.getBirthday().after(calendar.getTime()))
                // 按生日排序
                .sorted(Comparator.comparing(User::getBirthday))
                // 增加年龄并输出结果
                .map(user -> {
                    Date newBirthday = DateUtils.addYears(user.getBirthday(), 1);
                    User newUser = new User(user.getName(), newBirthday);
                    log.info(newUser.toString());
                    return newUser;
                })
                .forEachOrdered(user -> log.info(user.toString()));
    }

    /**
     * 测试作业应用
     */
    public static void main(String[] args) {
        testStreamApply(createUsers());
    }
}

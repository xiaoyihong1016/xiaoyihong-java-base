package com.bosssoft.hr.train.j2se.collection;

import com.bosssoft.hr.train.j2se.pojo.entity.User;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * @ClassName: TreeSetDemo
 * @Description: 使用 TreeSet 存储 User，并使用比较器按照工号进行排序
 * @Author: Xiao Yihong
 * @Date: 2023-2-19 18:50
 */
@Slf4j
public class TreeSetDemo extends BaseCollectionDemo{
    /**
     * 比较器Comparator 对象定义两个 User 对象的比较方式
     */
    private final Set<User> userTreeSet = new TreeSet<>(Comparator.comparing(User::getCode));

    /**
     * 利用自定义比较器实现初始化排序
     */
    private void sort(User[] array) {
        //传入的数组不为 null 或者长度不为 0
        if(null != array && array.length > 0) {
            //添加时按照比较器规则排序
            Collections.addAll(userTreeSet,array);
            User[] users = userTreeSet.toArray(new User[0]);
            log.info("log:"+"{}","排序成功");
        }
    }

    /**
     * 实现自定义排序
     */
    public void showSortResult() {
        // 调用父类方法构造
        User[] users = generateData();
        // 排序方法
        sort(users);
        // 输出
        for(User user :users) {
            log.info("用户: {}",user);
        }
    }

    public static void main(String[] args) {
        TreeSetDemo treeSetDemo = new TreeSetDemo();
        treeSetDemo.showSortResult();
    }

}

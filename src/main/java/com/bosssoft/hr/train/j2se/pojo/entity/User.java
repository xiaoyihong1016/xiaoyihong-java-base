package com.bosssoft.hr.train.j2se.pojo.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Objects;
import java.util.Set;

/**
 * @ClassName: User
 * @Description: 用户实体类
 * 该类继承自 BaseEntity 父类，并添加自己独有的字段。
 * 在数据中用户和角色是多对多关系通过中间表关联，因此用户内部有角色列表。
 * @Author: Xiao Yihong
 * @Date: 2023-2-19 15:30
 */
@Data
@NoArgsConstructor
public class User extends BaseEntity{
    /**
     * 用户所拥有的多个角色
     */
    private Set<Role> roles;

    /**
     * 工号
     */
    private String code;

    /**
     * 密码
     */
    private String password;

    /**
     * 名字
     */
    private String name;

    /**
     * 头像
     */
    private String profilePicture;

    /**
     * 性别 0为男，1为女
     */
    private byte sex;

    /**
     * 生日
     */
    private Date birthday;

    /**
     *  电话号码
     */
    private String tel;

    /**
     *  邮箱
     */
    private String email;

    /**
     *  其他
     */
    private String other;

    /**
     * 备注
     */
    private String remark;

    /**
     * 部门id
     */
    private Long departmentId;

    /**
     * 职位id
     */
    private Long positionId;

    /**
     * 用指定的ID、代码和名称构造一个新的用户对象。
     * @param id 用户ID
     * @param code 用户代码
     * @param name 用户名称
     */
    public User(Long id,String code,String name){
        this.id=id;
        this.code=code;
        this.name=name;
    }

    /**
     * 检查这个用户对象是否与另一个对象相等
     * 如果两个用户对象的id、code和name都相同，则认为是相等的
     * @param u 要比较的其他对象
     * @return true 如果用户对象与另一个对象相等；false 指定的用户对象与另一个对象不相等
     */
    @Override
    public boolean equals(Object u) {
        if (this == u) {return true;}
        if (u == null || getClass() != u.getClass()) {return false;}
        if (!super.equals(u)) {return false;}
        User user = (User) u;
        return Objects.equals(code, user.code) && Objects.equals(name, user.name) && Objects.equals(id,user.id);
    }

    /**
     * 返回该对象的哈希码值。这个实现是基于对象的
     * 超类的哈希码值，以及用户的ID、代码和名字的哈希码
     * @return 此对象的哈希码值
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), code, name, id);
    }

    /**
     * 输出 User 对象的信息
     * @return java.lang.String
     */
    @Override
    public String toString() {
        return "User{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}

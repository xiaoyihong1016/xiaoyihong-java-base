package com.bosssoft.hr.train.j2se.collection;

import com.bosssoft.hr.train.j2se.pojo.entity.User;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @ClassName: HashSetDemo
 * @Description: 使用 HashSet 存储用户对象，并对 HashSet 中的用户对象进行遍历
 * 在该类中重载了 User 类的 hashCode() 和 equals() 方法，用来判断 HashSet 中是否已经存在某个用户对象。
 * @Author: Xiao Yihong
 * @Date: 2023-2-19 17:40
 */
@Slf4j
public class HashSetDemo extends BaseCollectionDemo{
    /**
     * 定义userHashMap
     */
    private Set<User> userHashSet=new HashSet<>();

    /**
     * 对set中的用户进行遍历
     */
    public void visitByIterator() {
        //调用父类构造
        User[] users=generateData();
        userHashSet.addAll(Arrays.asList(users));
        /**
         * 构造的值是判断唯一的3个字段，即id,name,code
         */
        User user=new User(
                1L,
                String.format("BS%03d",1),
                "同学"+1
        );
        /**
         *  重复的对象在set中不存在，重复的依据依据hashcode和equal
         */
        userHashSet.add(user);
        log.info("用户数量 {}",userHashSet.size());
        //使用迭代器来访问元素
        Iterator<User> userIterator=userHashSet.iterator();
        while (userIterator.hasNext()){
            log.info("当前用户:{}",userIterator.next());
        }
    }

    /**
     * 打印集合中每个条目的键和值
     */
    public static void main(String[] args) {
        HashSetDemo hashSetDemo = new HashSetDemo();
        hashSetDemo.visitByIterator();
    }
}

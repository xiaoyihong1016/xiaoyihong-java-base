package com.bosssoft.hr.train.j2se.collection;

import com.bosssoft.hr.train.j2se.pojo.entity.User;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: HashMapDemo
 * @Description: 该类主要是遍历HashMap并输出其中所有User对象的信息
 * 代码通过调用generateData()方法生成一个User数组，将数组中的所有User对象添加到HashMap中，
 * 并使用迭代器遍历HashMap中的每个Entry（即键值对），输出每个User对象的信息。
 * @Author: Xiao Yihong
 * @Date: 2023-2-19 17:20
 */
@Slf4j
public class HashMapDemo extends BaseCollectionDemo {
    /**
     * 定义userHashMap
     */
    private final Map<Long, User> userHashMap = new HashMap<>();

    /**
     * 对map中的用户进行遍历
     */
    public void visitByEntrySet() {
        // 创建 HashMap 对象
        User[] users = generateData();
        for(User user: users){
            userHashMap.put(user.getId(),user);
        }
        // 遍历显示每个 User 对象的信息
        for (Map.Entry<Long, User> entry : userHashMap.entrySet()) {
            log.info("log:{}####{}", entry.getKey(), entry.getValue().toString());
        }
    }

    /**
     * 打印集合中每个条目的键和值
     */
    public static void main(String[] args) {
        HashMapDemo hashMapDemo = new HashMapDemo();
        hashMapDemo.visitByEntrySet();
    }
}

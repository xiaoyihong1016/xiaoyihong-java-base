package com.bosssoft.hr.train.j2se.file;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @Class: ExceptionCode
 * @Description:  实现文件上传云存储
 * @Author: Xiao Yihong
 * @Date: 2023/2/21 15:25
 */
@Slf4j
public class OssFileUitls {
    private static final String ENDPOINT = "oss-cn-beijing.aliyuncs.com";
    private static final String ACCESS_KEY_ID = "LTAI5tA56XVWL7TLxtWP5wXN";
    private static final String ACCESS_KEY_SECRET = "EXKQ9w0zxt2rp39aUxh9Swvm9qtlVs";
    private static final String BUCKET_NAME = "xiaoyh-java-basic";

    public static void upload(File file) {
        // 创建 OSS 客户端
        OSS ossClient = new OSSClientBuilder().build(ENDPOINT, ACCESS_KEY_ID, ACCESS_KEY_SECRET);
        try {
            // 构造上传请求
            PutObjectRequest putRequest = new PutObjectRequest(BUCKET_NAME, file.getName(), file);
            // 设置上传文件的元信息
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(file.length());
            putRequest.setMetadata(metadata);
            // 上传文件
            PutObjectResult putResult = ossClient.putObject(putRequest);
            // 打印上传结果
            log.info("Upload file {} to OSS succeeded", file.getName());
            log.info("ETag: {}", putResult.getETag());
            log.info("VersionId: {}", putResult.getVersionId());
        } catch (Exception e) {
            log.error("Upload file {} to OSS failed: {}", file.getName(), e.getMessage(), e);
            e.printStackTrace();
        } finally {
            // 关闭 OSS 客户端
            ossClient.shutdown();
        }
    }

    public static void main(String[] args) {
        // 创建一个临时文件进行上传
        File file = new File("test.txt");
        try {
            boolean isFileCreated = file.createNewFile();
            if (isFileCreated) {
                log.info("File {} created successfully", file.getName());
            } else {
                log.error("Failed to create file {}", file.getName());
            }
        } catch (Exception e) {
            log.error("Failed to create file {}: {}", file.getName(), e.getMessage(), e);
        }
        // 用临时文件调用上传方法
        upload(file);
        // 删除临时文件
        Path path = Paths.get(file.getAbsolutePath());
        try {
            Files.delete(path);
            log.info("File {} deleted successfully", file.getName());
        } catch (IOException e) {
            log.error("Failed to delete file {}", file.getName(), e);
        }
    }
}

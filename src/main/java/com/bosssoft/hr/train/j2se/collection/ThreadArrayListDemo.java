package com.bosssoft.hr.train.j2se.collection;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @ClassName: ThreadArrayListDemo
 * @Description:  使用同步 ArrayList 作为缓冲区解决生产者-使用者问题
 * @Author: Xiao Yihong
 * @Date: 2023-2-19 18:00
 */
@Slf4j
public class ThreadArrayListDemo {
    /**
     * 测试数据 公共数据模拟并发访问
     */
    private final List<String> productList=new ArrayList<>();

    /**
     * 测试生产者和消费者进行数据操作
     */
    public void startTesting() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(2);
        // Collections.synchronizedList方法将ArrayList转化为线程安全的列表
        List<String> syncProductList = Collections.synchronizedList(productList);
        //创建一个线程池，使用固定大小的线程池，大小为2
        ExecutorService executor = Executors.newFixedThreadPool(2);
        //提交生产者任务到线程池中执行
        executor.submit(new ProducerRunnable(syncProductList, countDownLatch));
        //提交消费者任务到线程池中执行
        executor.submit(new ConsumerRunnable(syncProductList, countDownLatch));
        //关闭线程池
        executor.shutdown();
        //等待所有任务执行完成
        countDownLatch.await();
    }

    /**
     * 生产者线程
     */
    public static class ProducerRunnable implements Runnable {
        /**
         *  计数
         */
        private final CountDownLatch countDownLatch;

        /**
         *  产品列别
         */
        private final List<String> productList;

        /**
         * @code ProducerThread类代表一个生产者线程，它将产品添加到一个共享列表中。
         * @param productList 产品的共享列表
         * @param countDownLatch 用于与其他线程同步的倒计时锁存器。
         */
        public  ProducerRunnable(List<String> productList, CountDownLatch countDownLatch){
            this.countDownLatch=countDownLatch;
            this.productList=productList;
        }

        /**
         * 该方法将新的产品无限期地添加到共享列表中，直到被打断。
         * 当线程完成后，该方法调用 countDownLatch.countDown() 方法来通知主线程该任务已经完成。
         */
        @Override
        public void run() {
            boolean keepAddingProducts = true;
            int i=1;
            try {
                while (keepAddingProducts) {
                    productList.add("product" + i);
                    log.info("加入 {}", "product" + i);
                    Thread.sleep(100);
                    i++;
                    if (i > 1000) {
                        keepAddingProducts = false;
                    }
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                log.warn("线程被中断");
            } finally {
                // 当线程执行结束时，调用 countDownLatch.countDown() 方法通知主线程
                countDownLatch.countDown();
            }
        }
    }

    /**
     * 消费者线程
     */
    public static class ConsumerRunnable implements Runnable{
        /**
         * 计数
         */
        private final CountDownLatch countDownLatch;

        /**
         * 产品列表
         */
        private final List<String> productList;

        /**
         * @code ConsumerRunnable类代表一个消费者线程，它将产品添加到一个共享列表中。
         * @param productList 产品的共享列表
         * @param countDownLatch 用于与其他线程同步的倒计时锁存器。
         */
        public ConsumerRunnable(List<String>productList, CountDownLatch countDownLatch){
            this.countDownLatch=countDownLatch;
            this.productList=productList;
        }

        /**
         * 该类表示从列表中删除元素
         */
        @Override
        public void run() {
            try {
                boolean running = true;
                while (running){
                    if(!productList.isEmpty()){
                        log.info("移除 {}",productList.get(0));
                        productList.remove(0);
                    }else{
                        log.info("没有数据可以删除");
                    }
                    Thread.sleep(100);
                    if (shouldStop()) {
                        running = false;
                    }
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                log.warn("线程被中断");
            } finally {
                this.countDownLatch.countDown();
            }
        }

        /**
         * 确定线程是否应该停止
         * @return true or false 如果同步产品列表为空，则返回true，否则返回false。
         */
        private boolean shouldStop() {
            return productList.isEmpty();
        }
    }

    /**
     * 启动ThreadArrayListDemo程序的主方法
     */
    public static void main(String[] args) throws InterruptedException {
        ThreadArrayListDemo demo = new ThreadArrayListDemo();
        demo.startTesting();
    }
}

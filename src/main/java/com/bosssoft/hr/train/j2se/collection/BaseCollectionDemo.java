package com.bosssoft.hr.train.j2se.collection;

import com.bosssoft.hr.train.j2se.pojo.entity.User;
import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: BaseCollectionDemo
 * @Description: 类的主要功能描述
 * @Author: Xiao Yihong
 * @Date: 2023-2-19 17:00
 */
@Slf4j
public class BaseCollectionDemo {
    /**
     * 初始化数据的上限，即用户的数量
     */
    private static final int MAX_LOOP_COUNT=10;

    /**
     * 生成测试数据
     * @return users 返回用户对象的数组
     */
    protected User[] generateData(){
        User[] users=new User[MAX_LOOP_COUNT];
        for(int i=0;i<MAX_LOOP_COUNT;i++){
            users[i]=new User(
                    (long) i,
                    String.format("BS%03d",i),
                    "同学"+i
            );
        }
        return users;
    }

    /**
     * 在用户对象的数组上进行迭代，并使用日志器记录每个用户对象的细节
     */
    public static class Main {
        public static void main(String[] args) {
            BaseCollectionDemo demo = new BaseCollectionDemo();
            User[] users = demo.generateData();
            for (User user : users) {
                log.info(user.toString());
            }
        }
    }
}

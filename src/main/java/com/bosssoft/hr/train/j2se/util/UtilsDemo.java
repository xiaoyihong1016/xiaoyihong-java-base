package com.bosssoft.hr.train.j2se.util;

import com.bosssoft.hr.train.j2se.pojo.entity.User;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @ClassName: UtilsDemo
 * @Author: Xiao Yihong
 * @Date: 2023-2-21 22:14
 */
public class UtilsDemo {
    private static final Logger log = LoggerFactory.getLogger(UtilsDemo.class);

    /**
     * 使用Collections.sort()将列表中的整数按升序排序，并记录每个元素
     */
    public void method1(){
        // 创建测试数据
        List<Integer> integerList = createList();
        Collections.sort(integerList);
        for (Integer integer : integerList) {
            log.info("元素：{}", integer);
        }
    }

    /**
     * 创建测试数据
     */
    private static final int MAX_NUMBER = 10;
    private List<Integer> createList() {
        List<Integer> integerList=new ArrayList<>();
        // 初始化终止
        Random random=new Random(1000);
        for(int i=0;i<MAX_NUMBER;i++){
            //生成100以内数字
            integerList.add(random.nextInt(100));
        }
        return integerList;
    }

    /**
     * 这个方法演示了Java中可用的各种操作数组和列表的实用方法
     */
    public void method2() {
        List<Integer> integerList = createList();
        // 检查给定的列表是否为空
        log.info("Testing if list is empty: {}", integerList.isEmpty());
        // 存在检查给定的数组是否包含符合给定谓词的元素
        String[] langs = {"java", "python", "c"};
        // 将两个数组合并为一个单一的列表
        String[] langs2 = {"java2", "python2", "c2"};
        List<String> resultList = Stream.concat(Arrays.stream(langs), Arrays.stream(langs2))
                .collect(Collectors.toList());
        log.info("Resultant list after merging: {}", resultList);
    }

    /**
     * 用来演示数组类中各种方法的用法，包括
     * 将一个数组转换为一个列表
     * 对数组进行排序
     * 对一个排序的数组进行二进制搜索
     * 复制一个指定长度的数组
     */
    public void method3() {
        Integer[] scores = new Integer[]{5, 6, 3, 4, 1};
        // 将数组转化为列表
        List<Integer> scoreList = new ArrayList<>(Arrays.asList(scores));
        // 对数组进行排序
        Arrays.sort(scores);
        // 在有序数组中进行二分查找
        int index = Arrays.binarySearch(scores, 2);
        if (index >= 0) {
            log.info("Index of 2: {}", index);
        } else {
            log.info("Element not found");
        }
        // 复制指定长度的数组
        Integer[] copyArray = Arrays.copyOf(scores, 3);
        log.info("Copy of scores: {}", (Object) copyArray);

        if (!scoreList.isEmpty()) {
            log.info("Sorted scores: {}", scoreList);
        }
    }

    /**
     * 搜索根目录下的Java文件。
     * @throws NullPointerException 在搜索过程中遇到空值
     * @throws IllegalArgumentException 传递给 FileUtils.listFiles() 的参数无效
     * @throws SecurityException 搜索Java文件的过程中发生安全漏洞
     */
    public void method4() {
        Collection<File> files;
        try {
            files = FileUtils.listFiles(new File("/"), new String[]{"java"}, true);
            log.info("Java文件的数量: {}", files.size());
        } catch (NullPointerException e) {
            log.error("Error: 搜索Java文件时发生空指针异常.");
        } catch (IllegalArgumentException e) {
            log.error("Error: 传递给FileUtils.listFiles()的参数无效.");
        } catch (SecurityException e) {
            log.error("Error: 搜索Java文件时发生安全异常.");
        }
    }

    /**
     * 该方法演示了如何使用StringUtils.isEmpty()来检查一个给定的字符串是否为空或空
     */
    public void method5() {
        // 判断空字符串
        boolean isEmpty = StringUtils.isEmpty("");
        log.info("Result for empty string: {}", isEmpty);

        // 判断null字符串
        String hello = null;
        isEmpty = StringUtils.isEmpty(hello);
        log.info("Result for null string: {}", isEmpty);
    }

    /**
     * 该方法演示了如何使用BeanUtils.copyProperties()将源对象的属性复制到目标对象。
     * @throws InvocationTargetException 目标方法抛出一个异常
     * @throws IllegalAccessException 类或初始化器不可访问
     */
    public void method6() throws InvocationTargetException, IllegalAccessException {
        User user = new User();
        user.setCode("BS0001");
        user.setName("同学1");

        User userCopy = new User();
        BeanUtils.copyProperties(user,userCopy);
    }

    /**
     * 使用Apache Commons Codec库中的DigestUtils类，对密码进行MD5和SHA3-256哈希运算
     */
    public void method7() {
        // MD5 加密  部分网站可以暴力破解
        String password="helloworld";
        byte[] bytePassword=password.getBytes(StandardCharsets.UTF_8);
        DigestUtils.md5(bytePassword);
        // SH3 加密 安全性更好不可逆
        DigestUtils.sha3_256(bytePassword);
    }

    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        UtilsDemo testClass = new UtilsDemo();
        testClass.method1();
        testClass.method2();
        testClass.method3();
        testClass.method4();
        testClass.method5();
        testClass.method6();
        testClass.method7();
    }
}


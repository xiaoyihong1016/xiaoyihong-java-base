package com.bosssoft.hr.train.j2se.pojo.entity;

import java.util.Objects;

/**
 * @ClassName: Role
 * @Description: 角色实体类
 * 该类继承自 BaseEntity 父类，并添加自己独有的字段。
 * @Author: Xiao Yihong
 * @Date: 2023-2-19 15:20
 */
public class Role extends BaseEntity{
    /**
     *  角色名
     */
    private final String name;

    /**
     *  角色代码
     */
    private final String code;

    /**
     * 用指定的ID、代码和名称构造一个新的角色对象。
     * @param id 角色ID
     * @param code 角色代码
     * @param name 角色名称
     */
    public Role(Long id,String code,String name) {
        this.id = id;
        this.name = name;
        this.code = code;
    }

    /**
     * 检查这个角色是否与另一个对象相等。
     * 当且仅当另一个对象也是一个角色，并且具有相同的id、name和code字段的值时返回true。
     * @param r 要比较的对象
     * @return true 如果指定的对象等于这个角色；false 指定的对象不等于这个角色
     */
    @Override
    public boolean equals(Object r) {
        if (this == r) {return true;}
        if (r == null || getClass() != r.getClass()) {return false;}
        if (!super.equals(r)) {return false;}
        Role role = (Role) r;
        return Objects.equals(name, role.name) && Objects.equals(code, role.code) && Objects.equals(id,role.id);
    }

    /**
     * 返回该对象的哈希代码值，该值是通过调用{@code Objects.hash()}方法计算出来的
     * 在这个{@code Role}对象的超类字段和{@code code}、{@code name}和{@code id}字段上计算出来的
     * @return {@code Role}对象的哈希码值
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), code, name, id);
    }

    /**
     *  输出 User 对象的信息
     *  @return java.lang.String
     */
    @Override
    public String toString() {
        return "Role{" +
                "name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", id=" + id +
                '}';
    }
}

package com.bosssoft.hr.train.j2se.exception;

/**
 * @Class: ExceptionCode
 * @Description: 定义了一个枚举类型，其中包含三个枚举常量
 * @Author: Xiao Yihong
 * @Date: 2023/2/21 09:11
 */
public enum ExceptionCode {
    //非法访问
    ILLEGAL_ACCESS("100","非法访问"),
    //数据库中列不存在
    COLUMN_NOT_EXIT("101","数据库中列不存在"),
    //参数不合理
    INVALID_PARAM("102","参数不合理");

    private final String code;
    private final String message;

    /**
     * 构建一个新的具有指定代码和消息的ExceptionCode对象。
     * @param code 异常代码
     * @param msg 异常消息
     */
    ExceptionCode(String code,String msg){
        this.code = code;
        this.message = msg;
    }

    public String getCode() {
        return code;
    }
    public String getMsg(){
        return message;
    }
}

package com.bosssoft.hr.train.j2se.exception;

/**
 * @Class: ExceptionDemo
 * @Description:  Java 异常处理机制的抛出异常、捕获和处理异常的演示和测试
 * @Author: Xiao Yihong
 * @Date: 2023/2/21 10:30
 */
public class ExceptionDemo {
    /**
     * method 1 运行时异常处理
     * 如果一个参数无效，则抛出一个BusinessException的自定义RuntimeException异常处理
     */
    public  void method1() {
        if(1 != 0) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
    }

    /**
     * method 2 非运行期异常处理
     * 如果一个参数无效，则抛出一个BusinessException的示例方法
     */
    public void method2() throws CustomException {
        throw new CustomException("抛出 CustomException 异常");
    }

    /**
     * method 2 的自定义异常类
     */
    public static class CustomException extends Exception {
        public CustomException(String message) {
            super(message);
        }
    }

    /**
     * 书写main函数测试异常
     */
    public static void main(String[] args) {
        ExceptionDemo exceptionDemo = new ExceptionDemo();
        // BusinessException从 RuntimeException继承 所以抛出异常不要求一定要捕获
        try {
            exceptionDemo.method1();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        try {
            // 这个因为抛出 Exception异常所以需要捕获如果不做着要求main也要抛出异常
            exceptionDemo.method2();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}